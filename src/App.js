import React, { Component } from 'react';
import { HashRouter, Route, Switch, Redirect, } from 'react-router-dom';
import './App.css';
import Sidebar from './Sidebar';
import routes from './routes';
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

class App extends Component {

  render() {
    return (
        <div className="App" id="outer-container">

            <Sidebar pageWrapId={'page-wrap'} outerContainerId={'outer-container'} />
        <HashRouter>
          <React.Suspense fallback={loading()}>
              <Switch>
                          {routes.map((route, idx) => {
                              return route.component ? (
                                  <Route
                                      key={idx}
                                      path={route.path}
                                      exact={route.exact}
                                      name={route.name}
                                      render={props => (
                                          <route.component {...props} />
                                      )} />
                              ) : (null);
                          })}
                          <Redirect from="/" to="/formlist" />
                      </Switch>
          </React.Suspense>
        </HashRouter>
        </div>
    );
  }
}

export default App;
