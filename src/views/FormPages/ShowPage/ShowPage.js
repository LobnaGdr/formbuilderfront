import React, { Component } from 'react';
import 'react-form-builder2/dist/app.css';
import {Col, Row} from "reactstrap";
import axios from "axios";
import Button from "@material-ui/core/Button";
import {Form} from "react-bootstrap";
class Forms extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Page :{titlePage:"", Form:{titleForm:"", items:[]}},
            response: [],

        };

    }


    componentDidMount(){

        this.getOne();
    }
    sumbit(){
        axios.post("http://localhost:8080/response/add",

            {idForm:this.state.Page.Form._id,
                IdPage : localStorage.getItem("idPage"),
                responseUSer: this.state.response})
            .then(data=>{
                console.log("data",data);
                window.location.href="/#/home"
            })
    }

    getOne() {
        fetch("http://localhost:8080/pages/search/" + localStorage.getItem("idPage"), {method: "GET"})
            .then(response => response.json())
            .then(data => {
                console.log("data", data);
                this.setState({Page: data})
            })
    }

    handleChange = (e) => {
      let  index=-1;
        if(this.state.response.length!==0){
        this.state.response.map((item,i) =>{
            if(item.name ===  e.target.name){
                index = i;
                return i;
            }
        });}
        if(index!==-1){
            this.state.response[index].value= e.target.value;
        }
        else{
this.state.response.push({"name":e.target.name, "value":e.target.value})
        }

    }
    retour(){

        window.location.href="/#/pageList"

    }
        render()
        {
            return (
                <div className="container">
                    <h1 >
                        {this.state.Page.titlePage}
                    </h1>
                     <div className="formulaire_Container">
                            <h2>
                                {this.state.Page.Form.titleForm}
                            </h2>
                         <Form className="formulaire">
                            <Form.Group style={{fontSize:"12px"}} as={Row} controlId="formHorizontalEmail">
                                <Form.Label column sm={2}>
                                    {this.state.Page.Form.description}
                                </Form.Label>

                            </Form.Group>
                             <br/>
                            {
                                this.state.Page.Form.items.map((item, i)=>{
                                    if(item.key=="number"){
                                        return (
                                            <div>

                                                <Form.Group key={i} as={Row} controlId="formHorizontalEmail">
                                                    <Form.Label column sm={2}> {item.label}</Form.Label>
                                                <br/>
                                                    <Form.Control className="inputVal inputNumber" placeholder="exemple: 10"  name={item.label} onChange={this.handleChange} />
                                                </Form.Group>
                                            </div>
                                        )
                                    }else if(item.key=="textarea"){
                                        return (<Form.Group as={Row} controlId="formHorizontalEmail">
                                            <Form.Label column sm={2}> {item.label}</Form.Label>
                                            <Col sm={10}>
                                                <Form.Control  className="inputVal textArea" placeholder="Réponse"   name={item.label} onChange={this.handleChange} />
                                            </Col>
                                        </Form.Group>)
                                    }else if(item.key=="range"){
                                        return (<Form.Group key={i} as={Row} controlId="formHorizontalEmail">
                                            <Form.Label column sm={2}> {item.label}</Form.Label>
                                            <Col sm={10}>
                                                <Form.Control className="inputVal" type="range" custom  name={item.label}
                                                              min="1" max="10"
                                                              onChange={this.handleChange}
                                                                />
                                            </Col>
                                        </Form.Group>)
                                    }else if(item.key=="checkbox"){
                                        return (
                                            <Form.Group key={i} as={Row} controlId="formHorizontalEmail">
                                                <Form.Label column sm={2}> {item.label}</Form.Label>
                                                <Col sm={10}>

                                                    {item.options.map((option,index) => {
                                                            console.log(option);
                                                            return ( <div key={index} className="mb-3">
                                                                <Form.Check
                                                                    name={item.label}
                                                                    onClick={this.handleChange}
                                                                    value={option}
                                                                    style={{ margin: "10px"}}
                                                                    inline
                                                                      
                                                                    label={option}
                                                                    type='checkbox'
                                                                    id={`inline-${index}`}
                                                                />
                                                            </div>)
                                                        }
                                                    )}
                                                </Col>
                                            </Form.Group>
                                        )
                                    }else if(item.key=="radio"){
                                        return (
                                            <Form.Group key={i}as={Row} controlId="formHorizontalEmail">
                                                <Form.Label column sm={2}> {item.label}</Form.Label>
                                                <Col sm={10}>

                                                    {item.options.map((option,index) => {
                                                            return ( <div key={index} className="mb-3">
                                                                <Form.Check
                                                                    name={item.label}
                                                                    onClick={this.handleChange}
                                                                    value={option}
                                                                    inline
                                                                    label={option}
                                                                    type='radio'
                                                                    style={{ margin: "10px"}}
                                                                    id={`radio-${index}`}
                                                                />
                                                            </div>)
                                                        }
                                                    )}
                                                </Col>
                                            </Form.Group>
                                        )
                                    }
                                    else if(item.key=="dropdown"){
                                        return  (   <div>
                                            <Form.Group key={i} controlId="exampleForm.SelectCustom">
                                                <Form.Label>{item.label} </Form.Label>
                                                <select  type="select"  className="select" as="select" dis>
                                                    <option value="">Choisir une reponse </option>
                                                    {item.options.map((option) => {
                                                        console.log(option);
                                                        return (<option name={item.label}
                                                                        onClick={this.handleChange}
                                                                        value={option}>{option}</option>)
                                                    })}
                                                </select>
                                            </Form.Group>
                                                <br/>
                                        </div>
                                        )
                                    } else{
                                        return(<div>
                                                <Form.Group key={i} controlId="exampleForm.SelectCustom">
                                                    <Form.Label column sm={2}> {item.label}</Form.Label>
                                                    <Col sm={10}>
                                                        <Form.Control className="inputVal" placeholder="Réponse"  name={item.label} onChange={this.handleChange}    />
                                                    </Col>
                                                </Form.Group>
                                            </div>
                                        )
                                    }


                                })
                            }

                         </Form>


                    <Button style={{marginLeft:"200px"}} type="submit" size="sm" color="primary" type="button" onClick={this.sumbit.bind(this)}
                    >ENOVOYER REPONSE </Button>
                    <Button type="submit" size="sm" color="red" type="button" onClick={this.retour.bind(this)}
                    >Retour </Button>

                     </div>
                </div>



            );
        }
    }
        export default Forms;
