import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from "@material-ui/core/Button";
import 'font-awesome/css/font-awesome.min.css';
import { faEye, faTrash , faFileArchive} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Paper from "@material-ui/core/Paper";
import TableContainer from "@material-ui/core/TableContainer";
import copy from "copy-to-clipboard";
class Tables extends Component {
    //declaration des variables
    constructor(){
        super();
        this.state={
            Forms:[],

        }



    }


    ///les fonction quis'excutent
    ///lorsque l'excution de la page
    componentDidMount(){
        this.getALL();
    }

    getALL(){
        fetch("http://localhost:8080/pages", {method : "GET"})
            .then(response=>response.json())
            .then(data=>{
                console.log("data",data);
                this.setState({Forms:data})
            })}

    remove(id){
        fetch("http://localhost:8080/pages/delete/"+id, {method : "DELETE"})
            .then(data=>{
                console.log("data",data);
                this.getALL();
            })
    }
    ///recuperer l'id
    handleClickDelete(e,id){
        e.preventDefault();
        console.log("id", id);
        this.remove(id);
    }
    handleClickShowPage(e,id){
        e.preventDefault();
        localStorage.setItem("idPage",id)
        window.location.href = "/#/pages/show"
    }
    handleClickShowResponse(e,id){

        e.preventDefault();
        console.log("id", id);
        ///variable public global
        localStorage.setItem("idPage",id)
        window.location.href = "/#/pages/showResponse"
    }
    render() {

        return (
            <div>
                <h1 align="center">Listes des Pages </h1>

                <TableContainer style={{width:"1200px",margin: "80px"}} component={Paper}>
                    <Table aria-label="a dense table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Titre</TableCell>
                            <TableCell>Description</TableCell>
                            <TableCell>Voir page</TableCell>
                            <TableCell>Responses </TableCell>
                            <TableCell>Supprimer  page</TableCell>

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            this.state.Forms.map((item,index) =>{

                                return(
                                    <TableRow key={index}>
                                        <TableCell>{item.titlePage}</TableCell>
                                        <TableCell>{item.description}</TableCell>
                                        <TableCell> <Button style={{color:"blue"}}onClick={e=>this.handleClickShowPage(e,item._id)} >
                                            <FontAwesomeIcon icon={faEye} />
                                        </Button></TableCell>
                                        <TableCell><Button onClick={e=>this.handleClickShowResponse(e,item._id)}><FontAwesomeIcon icon={faFileArchive} /></Button></TableCell>
                                        <TableCell><Button style={{color:"red"}}onClick={e=>this.handleClickDelete(e,item._id)}><FontAwesomeIcon icon={faTrash} /></Button></TableCell>
                                    </TableRow>
                                )})}
                    </TableBody>
                </Table>
                </TableContainer>
            </div>
        );
    }
}

export default Tables;

