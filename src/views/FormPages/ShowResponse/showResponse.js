import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from "@material-ui/core/Button";


class Tables extends Component {
    //declaration des variables
    constructor(){
        super();
        this.state={
            Reponses:[{responseUSer: []}],
            showing: false
        }

    }

    ///les fonction quis'excutent
    ///lorsque l'excution de la page
    componentDidMount(){
        this.getOne();
    }

    getOne() {
        fetch("http://localhost:8080/response/pages/" + localStorage.getItem("idPage"), {method: "GET"})
            .then(response => response.json())
            .then(data => {
                console.log("data", data);
                this.setState({Reponses: data})
            })
        console.log('thisss',this.state.Reponses)
    }

    getForm() {
        fetch("http://localhost:8080/forms/search/" + this.state.Reponses.idForm, {method: "GET"})
            .then(response => response.json())
            .then(data => {
                console.log("data", data);
                this.setState({Form: data})
            })
    }


    render() {
        if(this.state.Reponses.length !==0) {
let itemResponse=this.state.Reponses[0];
            const { showing } = this.state;
        return (

            <React.Fragment>

                <h1 align="center">Listes des reponses  </h1>
                <h2 style={{ marginLeft:"100px", fontFamily:"Roboto"}}>Le nombre des réponse est:<span style={{color:"blue"}}>{this.state.Reponses.length}</span> </h2>
                <Button style={{color:"blue", marginLeft:"100px"}} onClick={() => this.setState({ showing: !showing })}>Voir la listes des réponses </Button>
                { showing
                    ? <div>

                        <Table size="small" align="center">
                        <TableHead>
                            <TableRow>
                                {
                                    itemResponse.responseUSer.map((item) => {

                                        return (
                                            <TableCell>{item.name}</TableCell>
                                        )
                                    })
                                }
                            </TableRow>
                        </TableHead>
                        <TableBody>

                            {this.state.Reponses.map((itemResponse,index)=> {


                                return(
                                    <TableRow key={index}>
                                        {  itemResponse.responseUSer.map((item ) => {

                                            return (

                                                <TableCell>{item.value}</TableCell>
                                            )

                                        })}
                                    </TableRow>
                                )
                            })
                            }

                        </TableBody>
                    </Table>
                    </div>
                    : null
                }

            </React.Fragment>
        );}
        else{
            return <h1 align="center">Pas des reponses </h1>
        }
    }
}

export default Tables;

