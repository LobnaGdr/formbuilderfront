import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import axios from "axios/index";
import {Select} from '@material-ui/core';
import {Form} from "react-bootstrap";
import {Col, Row} from "reactstrap";
import copy from "copy-to-clipboard";
class Forms extends Component {
    constructor(props) {
        super(props);
        this.state = {
            form: [],
            titlePage: String,
            description: String,
            Form: {items:[]},
            vide:{items:[]}
        };
    }


    

    componentDidMount() {
        this.getALL();
    }

    CreatePage() {
        axios.post("http://localhost:8080/pages/addPage",

            {
                titlePage: this.state.titlePage,
                description: this.state.description,
                Form: this.state.Form
            })
            .then(data => {
                console.log("data", data);
               // copy("http://localhost:3000/#/pages");
                window.location.href = "/#/pageList"
            })
    }


    getALL() {
        fetch("http://localhost:8080/forms/searchall", {method: "GET"})
            .then(response => response.json())
            .then(data => {
                this.setState({form: data})
            })
    }

    rest() {
        window.location.href = "/#/pageList"
    }

    render() {
        return (
            <div className="container">
                    <h1 >
                     Ma Page!
                    </h1>
                <Form className="formulaire">
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="titre"
                            label="Titre"
                            name="titre"
                            autoComplete="titre"
                            autoFocus
                            value={this.state.titlePage} onChange={evt => this.setState({titlePage: evt.target.value})}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Description"
                            type="text"
                            id="description"
                            autoComplete="current-password"
                            value={this.state.description}
                            onChange={evt => this.setState({description: evt.target.value})}

                        />
                        <div>
                            <label htmlFor="form" label="Choisir un formulaire: "> Choisir un formulaire: </label>

                            <Select type="select" required name="formss" id="formss" value={this.state.Form} size="xs"

                                    onChange={evt => this.setState({Form: evt.target.value})}>
                                {<option value={this.state.vide} selected>Choisir un element</option>}
                                {
                                    this.state.form.map((item) => <option value={item}>{item.titleForm}</option>)
                                }
                            </Select>

                        </div>


                            <div className="formulaire_Container" >
                                <h1>
                                    {this.state.Form.titleForm}
                                </h1>

                                    <Form.Group as={Row} controlId="formHorizontalEmail">
                                        <Form.Label column sm={2}>
                                            {this.state.Form.description}
                                        </Form.Label>

                                    </Form.Group>
                                    {
                                        this.state.Form.items.map((item, i)=>{
                                        if(item.key=="number"){
                                            return (
                                                <div>

                                                    <Form.Group key={i} as={Row} controlId="formHorizontalEmail">
                                                        <Form.Label column sm={2}> {item.label}   </Form.Label>
                                                        <Form.Control className="inputVal inputNumber" placeholder="exemple: 10" disabled />
                                                    </Form.Group>
                                                </div>
                                            )
                                        }else if(item.key=="textarea"){
                                            return (<Form.Group as={Row} controlId="formHorizontalEmail">
                                                <Form.Label column sm={2}> {item.label}</Form.Label>
                                                <Col sm={10}>
                                                    <Form.Control  className="inputVal textArea" placeholder="Réponse" disabled />
                                                </Col>
                                            </Form.Group>)
                                        }else if(item.key=="range"){
                                            return (<Form.Group key={i} as={Row} controlId="formHorizontalEmail">
                                                <Form.Label column sm={2}> {item.label}</Form.Label>
                                                <Col sm={10}>
                                                    <Form.Control className="inputVal" type="range" custom disabled />
                                                </Col>
                                            </Form.Group>)
                                        }else if(item.key=="checkbox"){
                                            return (
                                                <Form.Group key={i} as={Row} controlId="formHorizontalEmail">
                                                    <Form.Label column sm={2}> {item.label}</Form.Label>
                                                    <Col sm={10}>

                                                        {item.options.map((option,index) => {
                                                                console.log(option);
                                                                return ( <div key={index} className="mb-3">
                                                                    <Form.Check

                                                                        inline
                                                                        disabled
                                                                        label={option}
                                                                        type='checkbox'
                                                                        id={`inline-${index}`}
                                                                    />
                                                                </div>)
                                                            }
                                                        )}
                                                    </Col>
                                                </Form.Group>
                                            )
                                        }else if(item.key=="radio"){
                                            return (
                                                <Form.Group key={i}as={Row} controlId="formHorizontalEmail">
                                                    <Form.Label column sm={2}> {item.label}</Form.Label>
                                                    <Col sm={10}>

                                                        {item.options.map((option,index) => {
                                                                return ( <div key={index} className="mb-3">
                                                                    <Form.Check
                                                                        inline
                                                                        disabled
                                                                        label={option}
                                                                        type='radio'
                                                                        id={`radio-${index}`}
                                                                    />
                                                                </div>)
                                                            }
                                                        )}
                                                    </Col>
                                                </Form.Group>
                                            )
                                        }
                                        else if(item.key=="dropdown"){
                                            return  (   <div>
                                                <Form.Group key={i} controlId="exampleForm.SelectCustom">
                                                    <Form.Label>{item.label} </Form.Label>
                                                    <select  type="select"  className="select" as="select" dis>
                                                        <option value="">Choisir une reponse </option>
                                                        {item.options.map((option) => {
                                                            console.log(option);
                                                            return (<option value={option}>{option}</option>)
                                                        })}
                                                    </select>
                                                </Form.Group>
                                            </div>)
                                        } else{
                                            return(<div>
                                                    <Form.Group key={i} controlId="exampleForm.SelectCustom">
                                                        <Form.Label column sm={2}> {item.label}</Form.Label>
                                                        <Col sm={10}>
                                                            <Form.Control className="inputVal" placeholder="Réponse" disabled />
                                                        </Col>
                                                    </Form.Group>
                                                </div>
                                            )
                                        }


                                    })
                                    }

                            </div>

                        <Button style={{marginLeft:"200px"}} type="submit" size="sm" color="primary" onClick={this.CreatePage.bind(this)}> <i
                            className="fa fa-dot-circle-o"></i> Ajouter</Button>
                        <Button type="reset" size="sm" color="danger" onClick={this.rest.bind(this)}><i
                            className="fa fa-ban"></i> Annuler</Button>
                </Form>
        </div>

        );
    }
}

export default Forms;
