import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';




class Login extends Component {
    ///declaration des variables
    constructor() {
        super();
        this.state = {
            password: "",
            passwordErr: "",
            login: "",
            loginErr: "",
            erreur: false
        }
    }

    validate = () => {

        let isError = false;

        const errors = {
            loginErr: "",
            passwordErr: "",
        }
        console.log("login ", this.state.login);
        console.log("pws ", this.state.password);
        const regex1 = /^[a-zA-Z0-9._-]+$/;
        if ((this.state.login === "") || (this.state.login.length > 15) || !regex1.test(this.state.login)) {
            isError = true;
            errors.loginErr = "Veuillez verifier votre login";
        }
        if ((this.state.password === "") || (this.state.password.length > 20)) {

            isError = true;
            errors.passwordErr = "veuillez verifier votre mot de passe";
        }
        if (isError) {
            this.setState({

                ...this.state,
                ...errors
            })
        }
        console.log("errrr ", isError)
        this.setState({
            erreur: isError
        })

        return isError;
    }

    login(e, idu) {
        console.log("state", this.state)
        let err = this.validate();
        if (!err) {
            axios.post("http://localhost:8080/user/auth", {
                userLogin: this.state.login,
                userPassword: this.state.password
            })
                .then(res => {
                    console.log("resultat", res.data)
                    if (res.data === null) {
                        alert("Vérifier votre login ou mot de passe")

                    } else {
                        alert("okkk")
                        //acceder à la page suivante dans la meme fenetre
                        localStorage.setItem("idu", res.data['-id'])
                        console.log("id ", res.data['-id'])
                        window.location.href = "/#/home/formList"

                    }


                })
        }
    }

    //s'éxcute lorsque l 'excution de  la page
    componentDidMount() {
        console.log("did")
    }

    //s'éxcute avant l 'excution de  la page
    componentWillMount() {
        console.log("will")
    }


    render() {
        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline/>
                <div >
                    <Avatar >
                        <LockOutlinedIcon/>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <form noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Login"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            value={this.state.login} onChange={evt=>this.setState({login:evt.target.value})}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            value={this.state.password} onChange={evt=>this.setState({password:evt.target.value})}

                        />

                        <Button color="primary" className="px-4" onClick={this.login.bind(this)}>Login</Button>

                    </form>
                </div>

            </Container>
        );
    }
}

export default Login;
