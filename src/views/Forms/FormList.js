import React, { Component } from 'react';
import Button from "@material-ui/core/Button";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { faEye, faTrash , faFileArchive} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Moment from 'moment';
class Tables extends Component {
  //declaration des variables
  constructor() {
    super();
    this.state = {
      Formss: [],


    }
  }



  ///les fonction quis'excutent
  ///lorsque l'excution de la page
  componentDidMount(){
    this.getALL();
  }
  //get la liste des formulaires
  getALL(){
    fetch("http://localhost:8080/forms/searchall", {method : "GET"})
      .then(response=>response.json())
      .then(data=>{
        console.log("data",data);
      this.setState({Formss:data})
      })}
      //pour supprimer un form
      remove(id){
        fetch("http://localhost:8080/forms/delete/"+id, {method : "DELETE"})
          .then(data=>{
            console.log("data",data);
          this.getALL();
          })
               }
               ///recuperer l'id
  handleClickDelete(e,id){
    e.preventDefault();
    console.log("id", id);
    this.remove(id);
  }
  handleShowForm(e,id){
    e.preventDefault();
    console.log( id);
    ///variable public global
    localStorage.setItem("idForm",id)
    window.location.href = "/#/formS/showForm"
  } handleClickShowRespForm(e,id){
    e.preventDefault();
    console.log( id);
    ///variable public global
    localStorage.setItem("idForm",id)
    window.location.href = "/#/pages/showResponseForm"
  }
  render() {

    return (
        <div  style={{background: "white"}} >
          <h1 align="center">Listes des formulaires </h1>
          <TableContainer style={{width:"1200px",margin: "80px"}} component={Paper}>
            <Table aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell >Titre</TableCell>
                <TableCell>Description</TableCell>
                <TableCell>Date de creation </TableCell>
                <TableCell>Voir </TableCell>
                <TableCell>Reponses </TableCell>
                <TableCell>Supprimer  </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {
                this.state.Formss.map((item,index) =>{

                  return(
                      <TableRow key={index}>
                    <TableCell>{item.titleForm}</TableCell>
                    <TableCell>{item.description}</TableCell>
                    <TableCell>{ Moment(item.dateCreation).format('DD-MM-YYYY')}</TableCell>
                   <TableCell><Button  style={{color:"blue"}}onClick={e=>this.handleShowForm(e,item._id)} ><FontAwesomeIcon icon={faEye} /></Button></TableCell>
                   <TableCell><Button onClick={e=>this.handleClickShowRespForm(e,item._id)} ><FontAwesomeIcon icon={faFileArchive} /></Button></TableCell>
                    <TableCell><Button style={{color:"red"}}onClick={e=>this.handleClickDelete(e,item._id)}><FontAwesomeIcon icon={faTrash} /></Button></TableCell>
                  </TableRow>
              )})}
            </TableBody>
            </Table>
          </TableContainer>
        </div>
    );
  }
}

export default Tables;

