import React, { Component } from 'react';
import 'react-form-builder2/dist/app.css';
import axios from "axios/index";
import {Col,  Row} from "reactstrap";
import {Form} from "react-bootstrap";
import Button from "@material-ui/core/Button";
import { faPlusSquare} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
class Forms extends Component {
  constructor(props) {
    super(props);
    this.state = {
        items :[],
        titleForm : String,
        description : String,
        type: String,
        optionInput:String,
        options: []
    };
  }

    _handleKeyDown(e) {
        if (e.key === 'Enter') {

            this.state.options.push(this.state.optionInput);
            this.setState({optionInput :""});
        }

    }



  CreateForm(){
      axios.post("http://localhost:8080/forms/add",

          {titleForm:this.state.titleForm,
              description : this.state.description,
              items: this.state.items})
          .then(data=> {
              console.log("data", data);
              window.location.href = "/#/home"
          })

  }

SelectElemnt(){
let item={};
      if(this.state.type=="number") {
          item={"key": "number",
              "label": this.state.placeholder
          }

      } else if(this.state.type=="textarea") {
          item={"key": "textarea",
              "label": this.state.placeholder
          }

      }
      else if(this.state.type == "checkbox"){

              item={"key": "checkbox",
                  "label": this.state.placeholder,
                  "options": this.state.options
              }
          }else if(this.state.type == "radio"){

              item={"key": "radio",
                  "label": this.state.placeholder,
                  "options": this.state.options
              }
          }
          else if(this.state.type=="dropdown"){
              item={"key": "dropdown",
                      "label": this.state.placeholder,
                      "options": this.state.options,
                      "placeholder": "Choisir un option"
                  }
          }  else if(this.state.type=="range"){
              item={"key": "range",
                      "label": this.state.placeholder,
                  }
          }

      else {
          item = {
              "key": "text",
              "label": this.state.placeholder,
          }

      }
      this.state.items.push(item);
      this.setState({placeholder :""});
      this.setState({type :""});
      this.setState({options :[]});


    }
    rest() {
        window.location.href = "/#/"
    }


  render() {
    return (

<div className="container">
    <div className="formulaire_Container">
                <h1 >
                   Mon Formulaire
                </h1>
                <Form className="formulaire">
                    <Form.Group as={Row} controlId="formHorizontalEmail">
                        <Form.Label  column sm={2}>
                            Ajouter le titre du formulaire
                        </Form.Label>
                        <Col sm={10}>
                            <Form.Control className="inputVal" type="text" placeholder="Titre"
                                value={this.state.titleForm} onChange={evt=>this.setState({titleForm:evt.target.value})}/>
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="formHorizontalEmail">
                        <Form.Label column sm={2}>
                            Ajouter la description du formulaire
                        </Form.Label>
                        <Col sm={10}>
                            <Form.Control className="inputVal textArea" type="text" placeholder="Description"
                             value={this.state.description} onChange={evt=>this.setState({description:evt.target.value})}/>
                        </Col>
                    </Form.Group>

                    {this.state.items.map((item,index)=>{
                    if(item.key=="number"){
                        return (
                            <div>

                                <Form.Group as={Row} controlId="formHorizontalEmail">
                            <Form.Label column sm={2}> {item.label}</Form.Label>
                            <Col sm={6}>
                                <Form.Control className="inputVal" placeholder="exemple: 10" disabled />
                            </Col>
                        </Form.Group>
                        </div>
                    )
                    }else if(item.key=="textarea"){
                        return (<Form.Group as={Row} controlId="formHorizontalEmail">
                            <Form.Label column sm={2}> {item.label}</Form.Label>
                            <Col sm={10}>
                                <Form.Control  className="inputVal textArea" placeholder="Réponse" disabled />
                            </Col>
                        </Form.Group>)
                    }else if(item.key=="range"){
                        return (<Form.Group as={Row} controlId="formHorizontalEmail">
                            <Form.Label column sm={2}> {item.label}</Form.Label>
                            <Col sm={10}>
                                    <Form.Control className="inputVal" type="range" custom  />
                            </Col>
                        </Form.Group>)
                    }else if(item.key=="checkbox"){
                        return (
                        <Form.Group as={Row} controlId="formHorizontalEmail">
                            <Form.Label column sm={2}> {item.label}</Form.Label>
                            <Col sm={10}>

                                {item.options.map((option,index) => {
                                console.log(option);
                                return ( <div key={index} className="mb-3">
                                    <Form.Check

                                        inline
                                        disabled
                                        label={option}
                                        type='checkbox'
                                        id={`inline-${index}`}
                                    />
                                </div>)
                            }
                        )}
                            </Col>
                        </Form.Group>
                            )
                    }else if(item.key=="radio"){
                        return (
                        <Form.Group as={Row} controlId="formHorizontalEmail">
                            <Form.Label column sm={2}> {item.label}</Form.Label>
                            <Col sm={10}>

                                {item.options.map((option,index) => {
                                console.log(option);
                                return ( <div key={index} className="mb-3">
                                    <Form.Check
                                        inline
                                        disabled
                                        label={option}
                                        type='radio'
                                        id={`radio-${index}`}
                                    />
                                </div>)
                            }
                        )}
                            </Col>
                        </Form.Group>
                            )
                    }
                    else if(item.key=="dropdown"){
                        return  (   <div>
                            <Form.Group controlId="exampleForm.SelectCustom">
                                <Form.Label>{item.label}</Form.Label>
                                <select className="inputVal select"  as="select" custom>
                                    <option value="">Choisir une reponse </option>
                                    {item.options.map((option) => {
                                        console.log(option);
                                        return (<option value={option}>{option}</option>)
                                    })}
                                </select>
                            </Form.Group>
                        </div>)
                    } else{
                        return(<div>
                            <Form.Group controlId="exampleForm.SelectCustom">
                            <Form.Label column sm={2}> {item.label}</Form.Label>
                            <Col sm={10}>
                                <Form.Control className="inputVal" placeholder="Réponse" disabled />
                            </Col>
                            </Form.Group>
                            </div>
                                )
                    }

                    })
                    }


                    <Form.Group controlId="exampleForm.SelectCustom">

                        <Form.Label>Choisir un élement </Form.Label>
                        <select className="inputVal select"  as="select" custom value={this.state.type} onChange={evt=>this.setState({type:evt.target.value})}>
                                <option value="0">Choisir un élement</option>
                                <option value="textarea">Paragraph</option>
                                <option value="dropdown">Dropdown</option>
                                <option value="checkbox">Checkboxes</option>
                                <option value="radio">Radio</option>
                                <option value="text">Label</option>
                                <option value="number">Number </option>
                                <option value="range">Range</option>
                        </select> <Button  size="sm" color="primary" onClick={this.SelectElemnt.bind(this)}>
                        <FontAwesomeIcon icon={faPlusSquare} /> Ajouter</Button>
                    </Form.Group>

                       <div>
                           <Form.Group as={Row} controlId="formHorizontalEmail">


                                   <Form.Control className="inputVal"  type="text" placeholder="Question" value={this.state.placeholder} size="xs"
                                       onChange={evt=>this.setState({placeholder:evt.target.value})}/>

                    <Col sm={10}>
                           {(() => {
                               if (this.state.type== "number") {
                                   return (
                                       <Form.Control className="inputVal"
                                           placeholder="exemple: 10" disabled
                                       />
                                   )
                               } else if (this.state.type== "textarea"){
                                   return (
                                       <Form.Control className="inputVal textArea"
                                           placeholder="Réponse" disabled
                                       />

                                   )
                               }else if(this.state.type=="range"){
                                   return (
                                           <Form.Control className="inputVal" type="range" custom />)
                               }else if(this.state.type=="checkbox"){
                                   return (
                                       <div>
                                   { this.state.options.map((option,index) => {
                                                       console.log('option', option);
                                                       return ( <div key={index} className="mb-3">
                                                           <Form.Check.Input type='checkbox' disabled />
                                                           <Form.Check.Label>{option}</Form.Check.Label>


                                                       </div>)
                                                   }
                                               )}
                                           <Form.Control className="inputVal" type="text" placeholder="Ajouter un option" value={this.state.optionInput}
                                                         onChange={evt=>this.setState({optionInput:evt.target.value})} onKeyDown={this._handleKeyDown.bind(this)}/>
                                       </div>
                                   )
                               }else if(this.state.type=="radio"){
                                   return (
                                       <div>
                                           { this.state.options.map((option,index) => {
                                                   console.log('option', option);
                                                   return ( <div key={index} className="mb-3">
                                                       <Form.Check.Input type='radio' disabled />
                                                       <Form.Check.Label>{option}</Form.Check.Label>


                                                   </div>)
                                               }
                                           )}
                                           <Form.Control className="inputVal" type="text" placeholder="Ajouter un option" value={this.state.optionInput}
                                                         onChange={evt=>this.setState({optionInput:evt.target.value})} onKeyDown={this._handleKeyDown.bind(this)}/>
                                       </div>
                                   )
                               }
                               else if(this.state.type=="dropdown"){
                                   return  (   <div>

                                           <Form.Control className="inputVal select"   as="select" custom>
                                               <option value="">Choisir une reponse </option>
                                               {this.state.options.map((option) => {
                                                   console.log(option);
                                                   return (<option value={option}>{option}</option>)
                                               })}
                                           </Form.Control>
                                       <Form.Control   className="inputVal" type="text" placeholder="Ajouter un option" value={this.state.optionInput}
                                                     onChange={evt=>this.setState({optionInput:evt.target.value})} onKeyDown={this._handleKeyDown.bind(this)}/>
                                   </div>)
                               } else{
                                   return(<div>

                                               <Col sm={10}>
                                                   <Form.Control className="inputVal" placeholder="Réponse" disabled />
                                               </Col>

                                       </div>
                                   )
                               }

                               })
                           ()}
                    </Col>
                           </Form.Group>


                    <br/>
                    <br/>
                           <Button  style={{marginLeft:"200px"}}className="btnn" type="submit" size="sm" color="primary" type="button" onClick={this.CreateForm.bind(this)}><i
                               className="fa fa-dot-circle-o"></i> Sauvagarder </Button>

                           <Button type="reset" size="sm" color="danger" onClick={this.rest.bind(this)}><i
                               className="fa fa-ban"></i> Annuler</Button>
                       </div>
                </Form>
</div>

</div>


    );
  }
}

export default Forms;
