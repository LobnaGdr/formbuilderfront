import React, {Component} from 'react';
import 'react-form-builder2/dist/app.css';
import {Col, Row} from "reactstrap";
import Button from "@material-ui/core/Button";
import {Form} from "react-bootstrap";
class Forms extends Component {
    constructor(props) {
        super(props);


        this.state = {
            Form: {titleForm: "", items: []}
        };
    }


    componentDidMount() {
        this.getOne();
    }

    getOne() {
        fetch("http://localhost:8080/forms/search/" + localStorage.getItem("idForm"), {method: "GET"})
            .then(response => response.json())
            .then(data => {
                console.log("data", data);
                this.setState({Form: data})
            })
    }

    retour() {
        window.location.href = "/#/home"
    }

    render() {
        return (
            <div className="container">
                <div className="formulaire_Container">
                    <h1>
                        {this.state.Form.titleForm}
                    </h1>
                    <Form className="formulaire">
                        <Form.Group as={Row} controlId="formHorizontalEmail">
                            <Form.Label column sm={2}>
                                {this.state.Form.description}
                            </Form.Label>

                        </Form.Group>
                        <br/>
                        {this.state.Form.items.map((item) => {
                            if (item.key == "number") {
                                return (
                                    <div>

                                        <Form.Group as={Row} controlId="formHorizontalEmail">
                                            <Form.Label column sm={2}> {item.label}</Form.Label>
                                            <br/>
                                            <Col sm={6}>
                                                <Form.Control className="inputVal" placeholder="exemple: 10" disabled/>
                                            </Col>
                                        </Form.Group>
                                    </div>
                                )
                            } else if (item.key == "textarea") {
                                return (<Form.Group as={Row} controlId="formHorizontalEmail">
                                    <Form.Label column sm={2}> {item.label}</Form.Label>
                                    <br/>
                                    <Col sm={10}>
                                        <Form.Control className="inputVal textArea" placeholder="Réponse" disbaled/>
                                    </Col>
                                </Form.Group>)
                            } else if (item.key == "range") {
                                return (<Form.Group as={Row} controlId="formHorizontalEmail">
                                    <Form.Label column sm={2}> {item.label}</Form.Label>
                                    <Col sm={10}>
                                        <Form.Control className="inputVal" type="range" custom disabled/>
                                    </Col>
                                </Form.Group>)
                            } else if (item.key == "checkbox") {
                                return (
                                    <Form.Group as={Row} controlId="formHorizontalEmail">
                                        <Form.Label column sm={2}> {item.label}</Form.Label>
                                        <Col sm={10}>

                                            {item.options.map((option, index) => {
                                                    console.log(option);
                                                    return (<div key={index} className="mb-3">
                                                        <Form.Check
                                                            style={{margin: "10px"}}
                                                            inline
                                                            disabled
                                                            label={option}
                                                            type='checkbox'
                                                            id={`inline-${index}`}
                                                        />
                                                    </div>)
                                                }
                                            )}
                                        </Col>
                                    </Form.Group>
                                )
                            } else if (item.key == "radio") {
                                return (
                                    <Form.Group as={Row} controlId="formHorizontalEmail">
                                        <Form.Label column sm={2}> {item.label}</Form.Label>
                                        <Col sm={10}>

                                            {item.options.map((option, index) => {
                                                    console.log(option);
                                                    return (<div key={index} className="mb-3">
                                                        <Form.Check style={{margin: "10px"}}
                                                                    inline
                                                                    disabled
                                                                    label={option}
                                                                    type='radio'
                                                                    id={`radio-${index}`}
                                                        />
                                                    </div>)
                                                }
                                            )}
                                        </Col>
                                    </Form.Group>
                                )
                            } else if (item.key == "dropdown") {
                                return (<div>
                                    <Form.Group controlId="exampleForm.SelectCustom">
                                        <Form.Label>{item.label}</Form.Label>
                                        <Form.Control className="inputVal" as="select" custom>
                                            <option value="">Choisir une reponse</option>
                                            {item.options.map((option) => {
                                                console.log(option);
                                                return (<option value={option}>{option}</option>)
                                            })}
                                        </Form.Control>
                                    </Form.Group>
                                </div>)
                            } else {
                                return (<div>
                                        <Form.Group controlId="exampleForm.SelectCustom">
                                            <Form.Label column sm={2}> {item.label}</Form.Label>
                                            <br/>
                                            <Col sm={10}>
                                                <Form.Control className="inputVal" placeholder="Réponse" disabled/>
                                            </Col>
                                        </Form.Group>
                                    </div>
                                )
                            }

                        })
                        }
                        <br/>
                        <Button style={{marginLeft: "200px"}} type="submit" size="sm" color="primary" type="button"
                        >ENOVOYER REPONSE
                        </Button>
                        <Button type="submit" size="sm" color="red" type="button" onClick={this.retour.bind(this)}
                        > Retour </Button>

                    </Form>
                </div>


            </div>


        );
    }
}

export default Forms;
