import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

class Tables extends Component {

    constructor(){
        super();
        this.state={
            Reponses:[{responseUSer: []}],

        }

    }

    ///les fonction quis'excutent
    ///lorsque l'excution de la page
    componentDidMount(){
        this.getOne();
    }

    getOne() {
        fetch("http://localhost:8080/response/form/" + localStorage.getItem("idForm"), {method: "GET"})
            .then(response => response.json())
            .then(data => {
                console.log("data", data);
                this.setState({Reponses: data})
            })
        console.log('thisss',this.state.Reponses)
    }



    render() {
        if(this.state.Reponses.length !==0) {
            let itemResponse = this.state.Reponses[0];

            return (
                <React.Fragment>
                    <h1 align="center">Listes des reponses </h1>
                    <Table size="small" align="center">
                        <TableHead>
                            <TableRow>
                                {
                                    itemResponse.responseUSer.map((item, index) => {

                                        return (
                                            <TableCell>{item.name}</TableCell>
                                        )
                                    })
                                }
                            </TableRow>
                        </TableHead>
                        <TableBody>

                            {this.state.Reponses.map((itemResponse, index) => {


                                return (
                                    <TableRow key={index}>
                                        {itemResponse.responseUSer.map((item) => {

                                            return (

                                                <TableCell>{item.value}</TableCell>
                                            )

                                        })}
                                    </TableRow>
                                )
                            })
                            }

                        </TableBody>
                    </Table>

                </React.Fragment>
            );
        }else{
            return <h1 align="center">Pas des reponses </h1>
        }
    }
}

export default Tables;

