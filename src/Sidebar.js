import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import './Sidebar.css'
export default props => {
    return (
        <Menu>
            <a className="menu-item" href="/FormList">
                Mes formulaires
            </a>
            <a className="menu-item" href="http://localhost:3000/#/pageList">
                Mes pages
            </a>
            <a className="menu-item" href="http://localhost:3000/#/forms/create">
                Création d'un formulaire
            </a>
            <a className="menu-item" href="http://localhost:3000/#/pages/create">
                Création d'une page
            </a>
        </Menu>
    );
};