import React from 'react';


const FormList = React.lazy(() => import('./views/Forms/FormList'));
const PageList = React.lazy(() => import('./views/FormPages/PagesList/PagesList'));
const CrateForm = React.lazy(() => import('./views/Forms/CreateForm/CrateForm'));
const ShowForm = React.lazy(() => import('./views/Forms/ShowForm/ShowForm'));
const ShowPage = React.lazy(() => import('./views/FormPages/ShowPage/ShowPage'));
const CreatePage = React.lazy(() => import('./views/FormPages/CreatePage'));
const ShowResponse = React.lazy(() => import('./views/FormPages/ShowResponse/showResponse'));
const ShowResponseForm = React.lazy(() => import('./views/Forms/ShowResponseForm/showResponseForm'));


// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [

    { path: '/formlist', exact: true, name: 'Listes des formulaires', component:FormList },
    { path: '/pageList', exact: true, name: 'Listes des pages', component:PageList },
    { path: '/forms/create', exact: true, name: 'Creer  formulaire', component:CrateForm },
    { path: '/forms/showForm', exact: true, name: 'voir  formulaire', component:ShowForm },
    { path: '/pages/create', exact: true, name: 'Creer  page', component:CreatePage },
    { path: '/pages/show', exact: true, name: 'voir page', component:ShowPage },
    { path: '/pages/showResponse', exact: true, name: 'voir page', component:ShowResponse },
    { path: '/pages/showResponseForm', exact: true, name: 'voir page', component:ShowResponseForm },
];

export default routes;